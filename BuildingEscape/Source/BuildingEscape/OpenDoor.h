// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Order of includes
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
// Generated is always last include.
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditInstanceOnly)
	float fOpenAngle = 60.0f;
	
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate;

	//UPROPERTY(EditAnywhere)
	AActor* ActorThatOpens;	//	Remember Pawn Inherits from Actor. Pawn IS AN Actor.	2 Directions to reference pawn from
	AActor* pActor;
	
	UPROPERTY(EditAnywhere)
	float DoorCloseDelay = 1.1f;
	float LastDoorOpenTime;
	
	void OpenDoor();
	void CloseDoor();
};